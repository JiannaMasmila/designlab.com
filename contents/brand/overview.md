---
name: Overview
---

<img class="d-block a-center gl-mb-7 img-25" src="/img/brand/gitlab-logo.svg" alt="GitLab logo" role="img" />

These guidelines set the standard for upholding a cohesive brand image. Our brand, like our product and company, has arrived to where it is today through the collaboration of numerous contributors and iterative processes. It exists as the creative expression of our company mission, vision, and [values](https://about.gitlab.com/handbook/values/).

At GitLab, we’ve created an environment where everyone can contribute, even when it comes to our brand. In 2015, the brand first emerged through the collaborative efforts of the wider GitLab community. After our company went public in 2021, we took a closer look at all its components and began a larger iteration to align our image with the next chapter of our journey. With this creative refresh we put the product at the center of our brand and refined each element to work seamlessly together.

Our product propels users into the future of DevOps. With that purpose in mind, we’ve designed our brand to embody the efficiency, innovation, modern-edge, and simplicity of our all-in-one DevOps Platform.

## Brand principles

Our designs are inspired by four key principles that allow us to create a multitude of visuals that maintain a consistent and cohesive brand identity. These brand principles embody the personality of our brand and are actuated by our product; they should be equally balanced when designing, each working together to tell an intentional story.

- **Human** - We are a people-first company with a community-centric product. We strive for open and approachable designs that prioritize inclusivity, accessibility, and transparency.
- **Reliable** - We maintain a consistent brand image that reinforces our trustworthiness and echoes the efficiency and productivity our product and company are known for.
- **Dynamic** - We are risk-takers that have paved the way with a comprehensive DevOps Platform. We create modern and innovative designs where multiple pieces work together to tell a meaningful story.
- **Clear** - We streamline the complex and show intricate concepts in a minimal way. We aim for simplified designs and messaging that are confident, clean, and informative, keeping many audiences in mind.

## Tone of voice

Our voice embodies our [values](https://about.gitlab.com/handbook/values/) and [mission](https://about.gitlab.com/company/mission/#mission). We openly welcome others into our collaborative space, sharing an inclusive perspective on GitLab’s vision.

Our brand principles extend to our voice and [communications](https://about.gitlab.com/handbook/communication/#writing-style-guidelines) as well. Consider how each characteristic works together to create a holistic narrative. For instance, our human-centric approach balances our ambitious edge; our concise messaging reinforces our trustworthiness; and so on. All facets are needed to fully represent who we are. Depending on the audience, it may make sense to lean into one area more than another.

| **Sample headline** | **Approach** |
| ------ | ------ |
| _The One DevOps Platform grows with you._ | **Human**. We invite others because we value inclusivity and transparency. We are approachable, relatable, and collaborative. |
| _Powerful, scalable, end-to-end automation is certain with GitLab._ | **Reliable**. We provide others with trustworthy and knowledgeable resources. We are helpful, confident, and consistent. |
| _A community of big ideas for better DevOps._ | **Dynamic**. We are dedicated to positive change. We are iterative, clever, and curious. |
| _GitLab. The One DevOps Platform._ | **Clear**. We are concise and straight to the point. We are efficient, informative, and organized. |

## Resources

**Logo**
- [Press kit](https://about.gitlab.com/press/press-kit/)

**Color**
- [RGB swatches](https://drive.google.com/file/d/1Ihb3DiRcm94KegtPFR3yXeTkXsoeGNJL/view?usp=sharing)
- [CMYK swatches](https://drive.google.com/file/d/1jHEZsVcdw4i-qJlgcCTfLBDn6XS08eVx/view?usp=sharing)

**Marketing illustrations**
- [Illustration library](https://drive.google.com/drive/folders/1GLoJ1Ua55vqTcYVfobcRunu9r7mQiLDq?usp=sharing) (Internal access only)
- [RGB swatches, secondary palette](https://drive.google.com/file/d/1kCcvxYMKPkDCEFQd6imQcHhFGC14Hgte/view?usp=sharing)
- [CMYK swatches - secondary palette](https://drive.google.com/file/d/1J2ZutCXZPJHQc9fgHvJYaVFSkOtlyn4t/view?usp=sharing)

**Marketing icons**
- [Icon library](https://drive.google.com/drive/folders/1dsRceA94H8CI0q7JAeWwEuWoNUuqdGq-?usp=sharing) (Internal access only)
- [Software development lifecycle](https://drive.google.com/drive/folders/15kLTAKngeVEE7dWbP471EVKfhF25kSnN?usp=sharing) (Internal access only)

**Photography**
- [Photo library](https://drive.google.com/drive/folders/1VHErs-KSNX1FIIVgXJR3OmIzwU7M4E1M?usp=sharing) (Internal access only)
- [Adobe stock](https://stock.adobe.com/)

**Templates**
- [General GitLab-themed deck template](https://docs.google.com/presentation/d/1xuw2RrjoSPx69p9v7aacrustmVto8cKWaFn_YK7Riug/edit#slide=id.g1287bf62b57_0_209) with [instructions](https://about.gitlab.com/handbook/tools-and-tips/#google-slides)
- [GitLab pitch deck template](https://docs.google.com/presentation/d/1vtFnh8DU6ZZzASTHg83UrhM6LJWqo5lq9mJDAY2ILpw/edit?usp=sharing)

**Quick links**
- [Corporate Marketing handbook](https://about.gitlab.com/handbook/marketing/corporate-marketing/)
- [Brand Design handbook](https://about.gitlab.com/handbook/marketing/corporate-marketing/brand-activation/brand-design/)
